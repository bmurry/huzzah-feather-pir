var Path = require('path');
var Hapi = require('hapi');

var Inert = require('inert');
var Good = require('good');
var Socket = require('socket.io');

 
var server = new Hapi.Server();


var options = {
    opsInterval: 1000,
    reporters: [{
        reporter: require('good-console'),
        events: { log: '*', response: '*' }
    }]
};


server.register(Inert,function () {});
server.register({register: Good,  options:options}, function () {});
server.connection({ port: 3000 });

var io = Socket(server.listener);

io.on('connection', function (socket) {

    socket.emit('connected...');
    
});

function sentinelHandler(req, reply){
    var data = req.payload;
    io.emit('sentinel', data);
    reply(0);
}
server.route([
    {
        method: 'POST',
        path: '/sentinel',
        config : {
            handler: sentinelHandler
        }
    },
    {
        method: 'GET'
        , path: '/{param*}'
        , config: { auth: false }
        , handler: {
          directory: {
            path: 'public'
            , listing: false
            , index: true
          }
        }
      }
]);

server.start();